const path = require("path");
const { ViteMinifyPlugin } = require("vite-plugin-minify");
const { default: htmlPurge } = require("vite-plugin-html-purgecss");
const ViteAMP = require("vite-plugin-amp");
import { partytownVite } from '@builder.io/partytown/utils';


export default  {
  root: path.resolve(__dirname, "src"),
  resolve: {
    alias: {
      "~bootstrap": path.resolve(__dirname, "node_modules/bootstrap"),
    },
  },
  server: {
    port: 8080,
    hot: true,
  },
  plugins: [ViteMinifyPlugin({}), htmlPurge(),partytownVite({
    dest: path.join(__dirname, 'src/dist', '~partytown'),
  }),]
};
